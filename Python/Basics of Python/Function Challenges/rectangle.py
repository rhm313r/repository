# Challenge 3
# Create a function that takes the width and height of the rectangle to be drawn as its input. 
# The function must then draw a rectangle with the dimensions specified.
 

import turtle

t = turtle.Turtle()
t.width(3)

def rectangle(width, height):
    for i in range(4):
        if i % 2 == 0: t.forward(width)
        else: t.forward(height)
        t.left(90)


rectangle(90, 220)
input()
