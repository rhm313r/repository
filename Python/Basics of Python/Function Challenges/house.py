# Challenge 4
# Use the previous functions that you have created to draw a house

import turtle

t = turtle.Turtle()
t.width(3)

def place(x, y):
    t.up()
    t.setpos(x, y)
    t.setheading(0)
    t.down()

def polygon(n_sides, length):
    for i in range(n_sides):
        t.forward(length)
        t.left(360/n_sides)

def rectangle(width, height):
    for i in range(4):
        if i % 2 == 0: t.forward(width)
        else: t.forward(height)
        t.left(90)

place(-150, -300)
polygon(4, 300)
place(-150, 0)
polygon(3, 300)
place(-100, -300)
rectangle(75, 150)
place(0, -125)
rectangle(100, 75)

turtle.done()
