# Challenge 1
# Use a Loop (it can be a For Loop or a While Loop) to draw a square using Turtle Graphics

import turtle

t = turtle.Turtle()
t.width(3)

for i in range(4):
    t.forward(100)
    t.left(90)

input()
