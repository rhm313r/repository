# Challenge 4
# Draw a 5 pointed star using turtle graphics
# Add some colours if you want to

import turtle

t = turtle.Turtle()
t.width(3)
t.color('red', 'yellow')
t.begin_fill()

for i in range(5):
    t.forward(200)
    t.right(144)

t.end_fill()

input()